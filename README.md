IdealReserveClient
==================

The Official Ideal Reserve Client

Maximum Wealth Through Maximum Stability

The Ideal Reserve is a cooperatively governed, decentralized, secure, and globally redundant currency that seeks to achieve minimum cost, truly instantaneous, inflation & deflation free transfers for individuals, businesses, & financial institutions with no downtime.

For details, please visit https://www.idealreserve.info/

Ideal Reserve Clearinghouse reserve-0.7.6.tar.xz SHA2: 1331d654a1778af615dd3d27ec20fd0c846b4c67e772f59a03c72a4a34e311af

WARNING!: If you run The Ideal Reserve Client from file rather than a website, be sure to do so in a newly opened incognito mode with no other web pages open in that mode.
